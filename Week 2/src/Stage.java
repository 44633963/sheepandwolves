import java.awt.*;

public class Stage{

    int x;
    int y;
    private Grid grid ;
    private Sheep Sheep;
    private Wolf Wolf;
    private Shepherd Shepherd;

    public Stage(int x, int y){
        this.x = x;
        this.y = y;
        this.grid = new Grid(10,10);
        this.Sheep = new Sheep(x+14*35,y+12*35,grid);
        this.Wolf = new Wolf(x+35*12, y+2*35,grid);
        this.Shepherd = new Shepherd(x+35*11, y+5*35, grid);
    }

    public void paint(Graphics g, Point mousePosition){
        Sheep.paint(g,Sheep.cell);
        Wolf.paint(g,Wolf.cell);
        Shepherd.paint(g,Shepherd.cell);
        grid.paint(g,mousePosition);
    }

    }

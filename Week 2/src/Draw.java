/**
 * Created by 44633963 on 7/08/2017.
 */
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

public class Draw extends JFrame implements Runnable {

    private class Canvas extends JPanel {

        private Stage stage;

        public Canvas() {
            setPreferredSize(new Dimension(1280, 720));

            stage = new Stage(10, 10);
        }

        @Override
            public void paint(Graphics g) {
            stage.paint(g, getMousePosition());
        }
    }






    public static void main(String[] args){

        Draw window = new Draw();
        window.run();
    }

    private Draw() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(new Canvas());
        this.pack();
        this.setVisible(true);
    }
    @Override
    public void run(){
        while(true) {
            this.repaint();
        }
    }
}
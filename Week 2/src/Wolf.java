import java.awt.*;

public class Wolf extends Character{
        public Wolf(int x, int y, Grid grid){
            super(x,y,grid);
            this.cell = new Cell(x,y);
        }

        public void paint(Graphics g, Cell cell){
            g.setColor(Color.RED);
            g.fillRect(cell.x,cell.y,35,35);
        }

 }


import java.awt.*;

public class Character {
    int x;
    int y;
    Grid grid;
    Cell cell;

    public Character(int x, int y, Grid grid){
        this.x = x;
        this.y = y;
        this.grid = grid;
        this.cell = new Cell(x,y);
    }


}



import java.awt.*;

public class Sheep extends Character {

    public Sheep(int x, int y, Grid grid){
        super(x,y,grid);
        this.cell = new Cell(x,y);
    }

    public void paint(Graphics g, Cell cell){
        g.setColor(Color.WHITE);
        g.fillRect(cell.x,cell.y,35,35);
    }


}